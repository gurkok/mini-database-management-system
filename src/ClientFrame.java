import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import javax.swing.filechooser.FileSystemView;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Scanner;

import static java.lang.Thread.sleep;

public class ClientFrame extends JFrame implements ActionListener {

    public JTextArea display;
    JButton button;
    JButton save;
    JButton load;
    JButton query;
    public JTextArea textArea;
    public JTabbedPane tabbedPane;
    public MyTable myTable;

    private final Client client;
    public final ClientReader clientReader;

    public ClientFrame(Client client) {

        this.client = client;

        setDefaultCloseOperation(DO_NOTHING_ON_CLOSE);

        addWindowListener(new WindowAdapter() {
            @Override
            public void windowClosed(WindowEvent e) {
                try {
                    client.sendMsg("exit");
                    client.stopConnection();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
                dispose();
            }

            @Override
            public void windowClosing(WindowEvent e) {
                try {
                    client.sendMsg("exit");
                    client.stopConnection();
                } catch (IOException exception) {
                    exception.printStackTrace();
                }
                dispose();
            }
        });

        setTitle("MiniABKR Client");
        setBounds(0, 0, 1000, 800);
        setLayout(null);
        Container cp = getContentPane();

        JPanel centerPanel = new JPanel();
        centerPanel.setBorder(new TitledBorder(new EtchedBorder(), "Code"));

        tabbedPane = new JTabbedPane();
        tabbedPane.setBounds(25,470,935,255);

        display = new JTextArea(22, 90);
        display.setEditable(true);
        JScrollPane scroll = new JScrollPane(display);
        scroll.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        centerPanel.add(scroll);
        centerPanel.setBounds(25,70,935,390);

        JPanel northPanel = new JPanel();
        northPanel.setBorder(new TitledBorder(new EtchedBorder()));

        load = new JButton("Load");
        load.addActionListener(this);
        northPanel.add(load);

        save = new JButton("Save");
        save.addActionListener(this);
        northPanel.add(save);

        button = new JButton("Run");
        button.addActionListener(this);
        northPanel.add(button);

        query = new JButton("Query designer");
        query.addActionListener(this);
        northPanel.add(query);

        northPanel.setBounds(25,15,935,40);

        JPanel southPanel = new JPanel();
        southPanel.setBorder(new TitledBorder(new EtchedBorder()));

        textArea = new JTextArea(13, 90);
        textArea.setEditable(false);
        textArea.setText("");
        JScrollPane scrollPane = new JScrollPane(textArea);
        scrollPane.setVerticalScrollBarPolicy(ScrollPaneConstants.VERTICAL_SCROLLBAR_ALWAYS);
        southPanel.add(scrollPane);
        southPanel.setBounds(25,470,935,255);

        myTable = new MyTable();

        tabbedPane.add("Output", southPanel);
        tabbedPane.add("TableView", myTable);


        cp.add(northPanel);
        cp.add(centerPanel);
        cp.add(tabbedPane);

        setVisible(true);

        clientReader = new ClientReader(client.getClientSocket(),this, client);
        clientReader.start();

    }


    @Override
    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(button)) {

            textArea.setText("");
            String[] commands = new String[0];
            if (display.getSelectedText() != null) {
                commands = display.getSelectedText().split(";");
            } else {
                commands = display.getText().split(";");
            }
            for (String command : commands) {
                command = command.trim().replaceAll("\n+"," ");
                command = command.trim().replaceAll(" +"," ");
                if (command.toUpperCase().equals("STOP") || command.toUpperCase().equals("EXIT")) {
                    try {
                        client.sendMsg(command);
                        client.stopConnection();
                    } catch (IOException exception) {
                        exception.printStackTrace();
                    }

                    dispose();
                } else {
                    if (!command.equals("")) {
                        if(command.toUpperCase().startsWith("SELECT"))
                            clientReader.isQuery = true;
                        client.sendMsg(command);
                    }
                }
                try {
                    sleep(100);
                } catch (InterruptedException interruptedException) {
                    interruptedException.printStackTrace();
                }
            }
        }
        if (e.getSource().equals(save)) {
            String z = display.getText();
            JFileChooser fileChooser = new JFileChooser(FileSystemView.getFileSystemView());
            fileChooser.setCurrentDirectory(new File(".\\codes"));

            int x = fileChooser.showDialog(null, null);

            if (x == JFileChooser.APPROVE_OPTION) {
                File f = fileChooser.getSelectedFile();
                try {
                    FileWriter fileWriter = new FileWriter(f);
                    fileWriter.write(z);
                    fileWriter.close();
                    textArea.setText("file saved");
                } catch (IOException ioException) {
                    ioException.printStackTrace();
                    textArea.setText("History cannot be saved");
                }

            } else {
                textArea.setText("Saving operation cancelled");
            }

        }
        if(e.getSource().equals(load)){
            JFileChooser fileChooser = new JFileChooser(FileSystemView.getFileSystemView());
            fileChooser.setCurrentDirectory(new File(".\\codes"));

            int x=fileChooser.showOpenDialog(null);

            if(x==fileChooser.APPROVE_OPTION){
                File f=fileChooser.getSelectedFile();
                String s = "";
                try {
                    Scanner scanner=new Scanner(f);
                    while(scanner.hasNextLine()){
                        s=s+scanner.nextLine()+"\n";
                    }
                } catch (FileNotFoundException fileNotFoundException) {
                    fileNotFoundException.printStackTrace();
                }
                display.setText(s);
                textArea.setText("file loaded");
            }
            else{
                textArea.setText("the user cancelled the operation");
            }
        }
        if(e.getSource().equals(query)){
            clientReader.runQueryDesigner = true;
            client.sendMsg("get tables");
        }
    }
}
