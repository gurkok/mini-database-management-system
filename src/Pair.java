public class Pair<T, K> {

    T first;
    K second;

    public Pair(T first, K indexName) {
        this.first = first;
        this.second = indexName;
    }

    public T getFirst() {
        return first;
    }

    public void setFirst(T first) {
        this.first = first;
    }

    public K getSecond() {
        return second;
    }

    public void setSecond(K second) {
        this.second = second;
    }

    public void setValue(T attributeIndex, K indexName) {
        this.first = attributeIndex;
        this.second = indexName;
    }

    public Pair<T, K> getValue() {
        return this;
    }
}
