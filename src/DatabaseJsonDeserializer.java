import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.DeserializationContext;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.deser.std.StdDeserializer;

import java.io.IOException;
import java.util.ArrayList;

public class DatabaseJsonDeserializer extends StdDeserializer<ArrayList<Database>> {

    protected DatabaseJsonDeserializer() {
        this(null);
    }

    protected DatabaseJsonDeserializer(Class<?> vc) {
        super(vc);
    }

    @Override
    public ArrayList<Database> deserialize(JsonParser jsonParser, DeserializationContext deserializationContext) throws IOException {

        ArrayList<Database> databases = new ArrayList<>();
        JsonNode root = jsonParser.getCodec().readTree(jsonParser);
        JsonNode jsonNodeDatabases = root.get("databases");
        if (jsonNodeDatabases != null) {
            for (JsonNode jsonNode : jsonNodeDatabases) {
                String databaseName = jsonNode.get("databaseName").asText();
                Database database = new Database(databaseName);
                ArrayList<Table> tables = new ArrayList<>();
                ArrayList<Index> indexes = new ArrayList<>();

                JsonNode tablesArray = jsonNode.get("tables");
                if (tablesArray != null) {
                    for (final JsonNode table : tablesArray) {
                        ArrayList<Attribute> attributes = new ArrayList<>();
                        JsonNode attributesArray = table.get("Structure");
                        if (attributesArray != null) {
                            for (final JsonNode attribute : attributesArray) {
                                boolean isNull = attribute.get("isnull").asBoolean();
                                boolean isPrimaryKey = attribute.get("isPrimaryKey").asBoolean();
                                boolean isUnique = attribute.get("isunique").asBoolean();
                                String type = attribute.get("type").asText();
                                String attributeName = attribute.get("attributeName").asText();
                                String foreignTable = attribute.get("foreignTable").asText();
                                String foreignAttribute = attribute.get("foreignAttribute").asText();
                                attributes.add(new Attribute(attributeName, type, isNull, isPrimaryKey, isUnique, foreignTable, foreignAttribute));
                            }
                            String tableName = table.get("tableName").asText();
                            //Integer rowLength = table.get("rowLength").asInt();
                            tables.add(new Table(attributes, tableName));
                        }
                    }
                }
                JsonNode indexesArray = jsonNode.get("indexes");
                if (indexesArray != null) {
                    for (final JsonNode index : indexesArray) {
                        String tableName = index.get("tableName").asText();
                        boolean isUnique = index.get("isUnique").asBoolean();
                        int keyLength = index.get("keyLength").asInt();
                        String indexName = index.get("indexName").asText();
                        String attributeName = index.get("indexAttributeName").asText();
                        indexes.add(new Index(tableName, isUnique, keyLength, indexName, attributeName));
                    }
                }
                // Itt csak egy darab Databaset kene visszateriteni
                database.setTables(tables);
                database.setIndexes(indexes);
                databases.add(database);
            }
        }
        return databases;
    }
}
