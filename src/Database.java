import java.util.ArrayList;

public class Database {

    private final String databaseName;
    private ArrayList<Table> tables;
    private ArrayList<Index> indexes;

    public Database(String databaseName) {

        this.databaseName = databaseName;
        tables = new ArrayList<>();
        indexes = new ArrayList<>();
    }

    public void addTable(String tableName, String data) {

        String[] attrs = data.split(", ");

        ArrayList<Attribute> attributes = new ArrayList<>();
        ArrayList<String> attributeNames = new ArrayList<>();

        boolean alreadyPrimaryKey = false;
        boolean isPrimaryKey = false;

        for (String string : attrs) {

            String[] attrData = string.split(" ");
            String name = attrData[0];
            String type = attrData[1];
            String foreignTable = "";
            String foreignAttribute = "";
            boolean notNull = false;
            boolean isUnique = false;
            isPrimaryKey = false;

            if (attrData.length >= 4 && attrData[2].equalsIgnoreCase("NOT") && attrData[3].equalsIgnoreCase("NULL")) {
                notNull = true;
                if (attrData.length == 5 && attrData[4].equalsIgnoreCase("UNIQUE")) {
                    isUnique = true;
                }
            }
            if (attrData.length == 3 && attrData[2].equalsIgnoreCase("UNIQUE")) {
                isUnique = true;
            }
            if (attrData.length >= 4 && attrData[2].toUpperCase().equals("PRIMARY") && attrData[3].toUpperCase().equals("KEY")) {
                if (alreadyPrimaryKey) {
                    throw new ArithmeticException("More than one primary key!!");
                } else {
                    isPrimaryKey = true;
                    alreadyPrimaryKey = true;
                }
            }
            if (attrData.length == 5 && attrData[2].equalsIgnoreCase("REFERENCES")) {
                foreignTable = attrData[3];
                foreignAttribute = attrData[4];
                //foreignAttribute = foreignAttribute.substring(1, foreignAttribute.length() - 1);
            }
            attributes.add(new Attribute(name, type, notNull, isPrimaryKey, isUnique, foreignTable, foreignAttribute));

            attributeNames.add(name);
        }

        for (int i = 0; i < attributeNames.size() - 1; i++) {
            for (int j = i + 1; j < attributeNames.size(); j++) {
                if (attributeNames.get(i).equals(attributeNames.get(j))) {
                    throw new ArithmeticException("ERROR");
                }
            }
        }

        if (!alreadyPrimaryKey) {
            throw new ArithmeticException("No primary key!");
        }

        tables.add(new Table(attributes, tableName));

    }

    public void dropTable(String tableName) {

        for (Table temp : tables) {

            if (temp.getTableName().equals(tableName)) {
                tables.remove(temp);
                break;
            }

        }

    }

    public void addIndex(boolean isUnique, String indexName, String tableName, String data) {

        int keyLength = data.length();

        Table tempTable = new Table(new ArrayList<>(), "");

        for (Table table : tables) {
            if (table.getTableName().equals(tableName)) {
                tempTable = table;
                break;
            }
        }


        boolean oke = false;
        for (Attribute attribute : tempTable.getAttributes()) {
            if (attribute.getAttributeName().equals(data)) {
                oke = true;
                break;
            }
        }

        if (!oke) {
            throw new ArithmeticException("Attribute not present in table!");
        }


        indexes.add(new Index(tableName, isUnique, keyLength, indexName, data));

    }

    public String getDatabaseName() {
        return databaseName;
    }

    public ArrayList<Table> getTables() {
        return tables;
    }

    public ArrayList<Index> getIndexes() {
        return indexes;
    }

    public void setTables(ArrayList<Table> tables) {
        this.tables = tables;
    }

    public void setIndexes(ArrayList<Index> indexes) {
        this.indexes = indexes;
    }

    @Override
    public String toString() {
        return "Database{" +
                "databaseName='" + databaseName + '\'' +
                ", tables=" + tables +
                ", indexes=" + indexes +
                "}\n";
    }
}
