import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class Output {
    private List<String> colNames = new ArrayList<>();
    private List<String> colValues = new ArrayList<>();

    public Output(String names) {
        colNames = Arrays.asList(names.split(" "));
    }

    public Output(String names, String values) {
        colNames = Arrays.asList(names.split(" "));
        colValues= Arrays.asList(values.split(" "));
    }

    public String getName(int i) {
        return colNames.get(i);
    }

    public int getSize(){
        return colNames.size();
    }

    public String getVal(int i) {
        if(i >= colValues.size())
            return "";
        return colValues.get(i);
    }

    public int valSize(){
        return colValues.size();
    }

    public List<String> getRow(){
        return colValues;
    }

    public String[] getVals(){
        return (String[]) colValues.toArray();
    }
}
