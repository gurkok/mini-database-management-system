import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.Version;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.module.SimpleModule;
import com.mongodb.BasicDBObject;
import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Filters;
import com.mongodb.client.model.UpdateOptions;
import org.bson.Document;
import org.bson.conversions.Bson;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.*;


public class Server {

    private ServerSocket serverSocket;
    private Socket clientSocket;
    private PrintWriter out;
    private BufferedReader in;
    private MongoDatabase database;
    private ArrayList<Database> databases;
    private Database currentDatabase;
    File file = new File("miniabkr.json");
    File insertFile = new File("insert.txt");

    String getCommand(String[] utasitasok) {
        if (utasitasok[0].equalsIgnoreCase("CREATE") && utasitasok[1].equalsIgnoreCase("DATABASE")) {
            return "CREATE DATABASE";
        }
        if (utasitasok[0].equalsIgnoreCase("USE")) {
            return "USE";
        }
        if (utasitasok[0].equalsIgnoreCase("DROP") && utasitasok[1].equalsIgnoreCase("DATABASE")) {
            return "DROP DATABASE";
        }
        if (utasitasok[0].equalsIgnoreCase("CREATE") && utasitasok[1].equalsIgnoreCase("TABLE")) {
            return "CREATE TABLE";
        }
        if (utasitasok[0].equalsIgnoreCase("DROP") && utasitasok[1].equalsIgnoreCase("TABLE")) {
            return "DROP TABLE";
        }
        if (utasitasok[0].equalsIgnoreCase("CREATE") && (utasitasok[2].equalsIgnoreCase("INDEX") || utasitasok[1].equalsIgnoreCase("INDEX")) && utasitasok.length >= 4) {
            return "CREATE INDEX";
        }
        if (utasitasok[0].equalsIgnoreCase("INSERT") && utasitasok[1].equalsIgnoreCase("INTO") && utasitasok[4].equalsIgnoreCase("VALUES")) {
            return "INSERT INTO TABLE";
        }
        if (utasitasok[0].equalsIgnoreCase("DELETE") && utasitasok[1].equalsIgnoreCase("FROM") && utasitasok[3].equalsIgnoreCase("WHERE")) {
            return "DELETE FROM TABLE";
        }
        if (utasitasok[0].equalsIgnoreCase("SELECT") && utasitasok[2].equalsIgnoreCase("FROM") && !Arrays.asList(utasitasok).contains("JOIN") && !Arrays.asList(utasitasok).contains("GROUP") && !containsAggr(utasitasok)) {
            return "SELECT FROM TABLE";
        }
        if (utasitasok[0].equalsIgnoreCase("INSERT") && utasitasok[1].equals("FROM") && utasitasok[2].equals("FILE")) {
            return "INSERT FROM FILE";
        }
        if (utasitasok[0].equalsIgnoreCase("SELECT")) {
            return "SELECT INNER JOIN";
        }
        if (utasitasok[0].equalsIgnoreCase("get") && utasitasok[1].equalsIgnoreCase("tables")) {
            return "GET TABLES";
        }
        return "DEFAULT";
    }

    public void start(int port) throws IOException {
        serverSocket = new ServerSocket(port);
        databases = new ArrayList<>();
        ObjectMapper mapper = new ObjectMapper();
        SimpleModule module = new SimpleModule();
        module.addDeserializer(ArrayList.class, new DatabaseJsonDeserializer());
        mapper.registerModule(module);

        boolean serverStatus = true;

        if (file.length() != 0) {
            databases = mapper.readValue(file, ArrayList.class);
        }
        System.out.println(databases);
        MongoClientURI uri = new MongoClientURI("mongodb+srv://m001-student:m001-mongodb-basics@sandbox.w7qju.mongodb.net/test");
        try (MongoClient mongoClient = new MongoClient(uri)) {

            while (serverStatus) {

                try {
                    clientSocket = serverSocket.accept();
                } catch (Exception e) {
                    e.printStackTrace();
                }

                while (true) {
                    out = new PrintWriter(clientSocket.getOutputStream(), true);
                    in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
                    String command = in.readLine();

                    if (command.equalsIgnoreCase("EXIT")) {
                        out.println("EXIT");
                        break;
                    }
                    if (command.equalsIgnoreCase("STOP")) {
                        out.println("Stopping server");
                        stop();
                        serverStatus = false;
                        break;
                    }
                    System.out.println(command);
                    String[] utasitasok = command.split(" ");
                    String utasitas = getCommand(utasitasok);
                    switch (utasitas) {
                        case "CREATE DATABASE" -> {
                            String databaseName = utasitasok[2];
                            boolean oke = true;
                            for (Database temp : databases) {
                                if (temp.getDatabaseName().equals(databaseName)) {
                                    oke = false;
                                    out.println("Error: " + "Database " + databaseName + " already exists!");
                                    break;
                                }
                            }
                            if (oke) {

                                currentDatabase = new Database(databaseName);

                                databases.add(currentDatabase);

                                database = mongoClient.getDatabase(currentDatabase.getDatabaseName());

                                out.println("Database " + databaseName + " created!");
                            }
                        }
                        case "USE" -> {
                            String databaseName = utasitasok[1];
                            boolean oke = false;
                            for (Database temp : databases) {
                                if (temp.getDatabaseName().equals(databaseName)) {
                                    oke = true;
                                    currentDatabase = temp;
                                    database = mongoClient.getDatabase(currentDatabase.getDatabaseName());
                                    out.println("Database " + databaseName + " is being used");
                                    break;
                                }
                            }
                            if (!oke) {
                                out.println("Error: " + "Database " + databaseName + " doesn't exist!");
                            }

                        }
                        case "DROP DATABASE" -> {
                            String databaseName = utasitasok[2];
                            boolean oke = false;
                            for (Database temp : databases) {
                                if (temp.getDatabaseName().equals(databaseName)) {
                                    oke = true;
                                    databases.remove(temp);
                                    database = mongoClient.getDatabase(databaseName);
                                    database.drop();
                                    out.println("Database " + databaseName + " dropped");
                                    break;
                                }
                            }
                            if (!oke) {
                                out.println("Error: " + "Database " + databaseName + " doesn't exist!");
                            }
                        }
                        case "CREATE TABLE" -> {

                            String tableName = utasitasok[2].split("\\(", 2)[0];

                            int indexAttr = command.indexOf('(') + 1;

                            String attrs = command.substring(indexAttr, command.length() - 1);

                            boolean oke = tableExists(tableName);

                            if (!oke) {
                                try {
                                    currentDatabase.addTable(tableName, attrs);
                                    out.println("Table " + tableName + " created");
                                } catch (Exception e) {
                                    out.println("Error: " + "The attributes are repeated or duplicate/not existing primary keys!");
                                }
                            } else {
                                out.println("Error: " + "Table " + tableName + " already exists!");
                            }
                        }
                        case "DROP TABLE" -> {

                            String tableName = utasitasok[2];

                            boolean oke = false;
                            for (Table temp : currentDatabase.getTables()) {
                                if (temp.getTableName().equals(tableName)) {
                                    oke = true;
                                    currentDatabase.dropTable(tableName);
                                    out.println("Table " + tableName + " dropped!");
                                    break;
                                }
                            }

                            if (!oke) {
                                out.println("Error: " + "Table " + tableName + " doesn't exist!");
                            }
                        }
                        case "CREATE INDEX" -> {

                            boolean isUnique = false;
                            String indexName = "";
                            String tableName = "";

                            if (utasitasok[1].equalsIgnoreCase("UNIQUE") && utasitasok[4].toUpperCase().equals("ON")) {
                                isUnique = true;
                                indexName = utasitasok[3];
                                tableName = utasitasok[5].split("\\(")[0];
                            }

                            if (utasitasok[1].equalsIgnoreCase("INDEX") && utasitasok[3].toUpperCase().equals("ON")) {
                                indexName = utasitasok[2];
                                tableName = utasitasok[4].split("\\(")[0];
                            }

                            String attributeName = command.split("\\(")[1];

                            attributeName = attributeName.substring(0, attributeName.length() - 1);

                            boolean oke2 = false;
                            for (Table table : currentDatabase.getTables()) {
                                if (table.getTableName().equals(tableName)) {
                                    oke2 = true;
                                    break;
                                }
                            }

                            if (!oke2) {
                                out.println("Error: " + "The given table doesn't exist " + tableName);
                            } else {

                                boolean oke = true;

                                for (Index index : currentDatabase.getIndexes()) {
                                    if (index.getIndexName().equals(indexName)) {
                                        oke = false;
                                        break;
                                    }
                                }


                                if (oke) {
                                    Attribute tempAttribute = getAttributeFromTable(attributeName, tableName);
                                    if (!tempAttribute.isPrimaryKey()) {
                                        if (!indexOnAttributeExists(attributeName, tableName, indexName)) {
                                            if (attributeFromTableExists(attributeName, tableName)) {
                                                currentDatabase.addIndex(isUnique, indexName, tableName, attributeName);
                                                insertNewIndexInMongo(indexName, attributeName, tableName);
                                            } else {
                                                out.println("Error: " + "Attribute " + attributeName + " non-existent in table " + tableName);
                                            }
                                        } else {
                                            out.println("Error: " + "There is already an index on " + attributeName);
                                        }
                                    } else {
                                        out.println("Error: " + "There is already an index on THE PRIMARY KEY " + attributeName);
                                    }
                                } else {
                                    out.println("Error: " + "Index already exists " + indexName);
                                }
                            }
                        }

                        case "INSERT INTO TABLE" -> {
                            insertIntoTable(utasitasok, utasitasok[3]);
                        }
                        case "DELETE FROM TABLE" -> {
                            String tableName = utasitasok[2];
                            if (tableExists(tableName)) {
                                boolean attributesExist = true;
                                for (int i = 4; i < utasitasok.length; i += 4) { //konstans
                                    if (!attributeFromTableExists(utasitasok[i], tableName)) {
                                        attributesExist = false;
                                        break;
                                    }
                                }
                                if (!attributesExist) {
                                    out.println("Error: " + "Some of the attributes don't exist");
                                } else {
                                    int rowsAffected = deleteFromTable(utasitasok, tableName);
                                    if (rowsAffected == 0) {
                                        out.println("Error: " + "Couldn't delete because of table constraints");
                                    } else {
                                        out.println(rowsAffected + " rows affected");
                                    }
                                }
                            } else {
                                out.println("Error: " + "Table " + tableName + " doesn't exist");
                            }
                        }
                        case "SELECT FROM TABLE" -> {
                            String tableName = utasitasok[3];
                            String[] attrNames;
                            if (!tableExists(tableName)) {
                                out.println("Error: " + "Table " + tableName + " doesn't exist");
                            } else {
                                boolean attrNameExists = true;
                                if (utasitasok[1].equals("*")) {
                                    attrNames = getAttrNamesFromTable(tableName);
                                } else {
                                    attrNames = utasitasok[1].split(",");
                                    for (String attrName : attrNames) {
                                        if (!attributeFromTableExists(attrName, tableName)) {
                                            out.println("Error: " + "The attribute " + attrName + " doesn't exist");
                                            attrNameExists = false;
                                            break;
                                        }
                                    }
                                }
                                if (attrNameExists) {
                                    for (int i = 5; i < utasitasok.length; i += 4) {
                                        String attrName = utasitasok[i];
                                        if (!attributeFromTableExists(attrName, tableName)) {
                                            out.println("Error: " + "The attribute " + attrName + " doesn't exist");
                                            attrNameExists = false;
                                            break;
                                        }
                                    }
                                }
                                if (attrNameExists) {
                                    ArrayList<Integer> attrNamesIndexes = getAttrNamesIndexes(tableName, attrNames);
                                    if (utasitasok.length >= 5) {
                                        ArrayList<String> primaryKeys = selectFromTable(tableName, utasitasok);
                                        writeoutSelectedAttrs(primaryKeys, tableName, attrNamesIndexes, attrNames);
                                    } else {
                                        writeoutAllSelectedAttrs(tableName, attrNamesIndexes, attrNames);
                                    }
                                }
                            }
                        }
                        case "INSERT FROM FILE" -> {
                            insertFromFile();
                        }
                        case "SELECT INNER JOIN" -> {
                            ArrayList<Pair<String, String>> tableNamePairs = new ArrayList<>();
                            boolean validData = true;
                            for (int i = 0; i < utasitasok.length; i++) {
                                if (utasitasok[i].equalsIgnoreCase("FROM")) {
                                    tableNamePairs.add(new Pair<>(utasitasok[i + 1], utasitasok[i + 2]));
                                    i += 2;
                                } else if (utasitasok[i].equalsIgnoreCase("INNER")) {
                                    if (!utasitasok[i + 1].equalsIgnoreCase("JOIN")) {
                                        out.println("Error: " + "WRONG SYNTAX");
                                        validData = false;
                                        break;
                                    } else {
                                        tableNamePairs.add(new Pair<>(utasitasok[i + 2], utasitasok[i + 3]));
                                    }
                                    i += 3;
                                }
                            }
                            if (validData) {
                                for (Pair<String, String> tablePair : tableNamePairs) {
                                    if (!tableExists(tablePair.first)) {
                                        validData = false;
                                        out.println("Error: " + "Table: " + tablePair.first + " doesn't exist");
                                        break;
                                    }
                                }
                                if (validData) {
                                    ArrayList<ArrayList<String>> primaryKeys = innerJoin(tableNamePairs, utasitasok);
                                    ArrayList<Pair<String, Pair<String, String>>> aggrAndColName = new ArrayList<>();
                                    ArrayList<Pair<Pair<String, Integer>, Pair<String, Integer>>> tablesAttrsInds = new ArrayList<>();
                                    ArrayList<Pair<Pair<String, Integer>, Pair<String, Integer>>> groupByTablesAttrsInds = new ArrayList<>();
                                    if (utasitasok[1].equalsIgnoreCase("*")) {
                                        tablesAttrsInds = getTablesAttrsInds(tableNamePairs);
                                    } else {
                                        String[] tableAttrs = utasitasok[1].split(",");
                                        String tableAbbr = "";
                                        String attrName = "";
                                        for (String tableAttr : tableAttrs) {
                                            if (tableAttr.contains("MIN") || tableAttr.contains("MAX") || tableAttr.contains("SUM") || tableAttr.contains("COUNT") || tableAttr.contains("AVG")) {
                                                tableAttr = tableAttr.substring(0, tableAttr.length() - 1);
                                                String[] parts = tableAttr.split("\\(");
                                                String[] attrAndTableName = parts[1].split("\\.");
                                                tableAbbr = attrAndTableName[0];
                                                attrName = attrAndTableName[1];
                                                String tableLongName = getTableNameAndIndFromPair(tableAbbr, tableNamePairs).first;
                                                if (attributeFromTableExists(attrName, tableLongName)) {
                                                    aggrAndColName.add(new Pair<>(parts[0], new Pair<>(tableLongName, attrName)));
                                                } else {
                                                    validData = false;
                                                    out.println("Error: " + attrName + " from table " + tableLongName + " doesn't exist");
                                                    break;
                                                }
                                            } else {
                                                tableAbbr = tableAttr.split("\\.")[0];
                                                attrName = tableAttr.split("\\.")[1];
                                            }
                                            getLongTableNameAndAttr(tableNamePairs, tablesAttrsInds, tableAbbr, attrName);
                                        }
                                        if (validData) {
                                            groupByTablesAttrsInds = getGroupByContents(tableNamePairs, utasitasok);
                                            if (groupByTablesAttrsInds == null) {
                                                validData = false;
                                            } else if (!groupByTablesAttrsInds.isEmpty() && !aggrAndColName.isEmpty()) {
                                                if (!allAttrsPresent(tablesAttrsInds, aggrAndColName, groupByTablesAttrsInds)) {
                                                    validData = false;
                                                }
                                            }
                                        }
                                    }
                                    if (validData) {
                                        writeoutAllSelectedAttrsJoin(primaryKeys, tablesAttrsInds, groupByTablesAttrsInds, aggrAndColName);
                                        out.println("Select command done");
                                    }
                                }
                            }
                        }
                        case "GET TABLES" -> {
                            if (currentDatabase != null) {
                                for (Table temp : currentDatabase.getTables()) {
                                    out.println(temp.getTableName());
                                }
                                out.println("Get tables done");
                            } else {
                                out.println("Error: " + "Please use a database");
                            }
                        }
                        default -> out.println("Error: " + "INVALID INPUT");
                    }
                    updateJson();
                }
            }
        }

    }

    public void stop() {
        updateJson();
        try {
            in.close();
            out.close();
            clientSocket.close();
            serverSocket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean containsAggr(String[] list) {
        for (String word : list) {
            if (word.matches("^.*MIN.*$") || word.matches("^.*MAX.*$") || word.matches("^.*AVG.*$") || word.matches("^.*SUM.*$") || word.matches("^.*COUNT.*$")) {
                return true;
            }
        }
        return false;
    }

    public void updateJson() {
        ObjectMapper mapper = new ObjectMapper();

        System.out.println("Json updated");
        try {
            JsonGenerator g = mapper.getFactory().createGenerator(new FileOutputStream(file));
            ObjectMapper objectMapper = new ObjectMapper();
            SimpleModule module =
                    new SimpleModule("DatabaseJsonSerializer", new Version(1, 0, 0, null, null, null));
            module.addSerializer(new DatabaseJsonSerializer());
            objectMapper.registerModule(module);
            objectMapper.writerWithDefaultPrettyPrinter().writeValue(file, databases);
            g.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void insertIntoTable(String[] utasitasok, String tempAttributeNames) {
        String tableName = utasitasok[2];
        if (tableExists(tableName)) {
            tempAttributeNames = tempAttributeNames.substring(1, tempAttributeNames.length() - 1);
            ArrayList<String> attributeNames = new ArrayList<>(Arrays.asList(tempAttributeNames.split(",")));
            if (!duplicateExists(attributeNames) && attributeInTable(tableName, attributeNames)) {
                String tempValueNames = utasitasok[5];
                tempValueNames = tempValueNames.substring(1, tempValueNames.length() - 1);
                insertValuesIfValid(tableName, attributeNames, tempValueNames);
            } else {
                out.println("Error: " + "Invalid attribute names or you entered a duplicate!");
            }
        } else {
            out.println("Error: " + "Table " + tableName + " doesn't exist");
        }
    }

    public void insertFromFile() throws FileNotFoundException {
        Scanner fileLines = new Scanner(insertFile);
        String firstRow = fileLines.nextLine();
        String[] utasitasok = firstRow.split(" ");
        String tableName = utasitasok[2];
        String tempAttributeNames = utasitasok[3];
        if (tableExists(tableName)) {
            tempAttributeNames = tempAttributeNames.substring(1, tempAttributeNames.length() - 1);
            ArrayList<String> attributeNames = new ArrayList<>(Arrays.asList(tempAttributeNames.split(",")));
            int primaryKeyIndex = getPrimaryKeyIndexFromArray(tableName, attributeNames);
            MongoCollection<Document> mongoCollection = database.getCollection(tableName);
            if (!duplicateExists(attributeNames) && attributeInTable(tableName, attributeNames)) {
                while (fileLines.hasNextLine()) {
                    String tempValueNames = fileLines.nextLine();
                    insertValuesFromFile(tableName, attributeNames, tempValueNames, primaryKeyIndex, mongoCollection);
                }
            } else {
                out.println("Error: " + "Invalid attribute names or you entered a duplicate!");
            }
        } else {
            out.println("Error: " + "Table " + tableName + " doesn't exist");
        }
    }


    public void insertValuesFromFile(String tableName, ArrayList<String> attributeNames, String tempValueNames,
                                     int primaryKeyIndex, MongoCollection<Document> mongoCollection) {
        ArrayList<String> valueNames = getValueNameList(tempValueNames);
        String concatedValues = concatValues(attributeNames, valueNames, tableName, primaryKeyIndex);
        if (concatedValues.equals("ERROR")) {
            out.println("Error: " + "Not null attributes should be assigned a value");
        } else {
            String mongoKey = valueNames.get(primaryKeyIndex);
            Document document;
            document = new Document("_id", mongoKey);
            document = document.append("value", concatedValues); //caninsert
            mongoCollection.insertOne(document);
            insertIntoIndexesAttrs(mongoKey, valueNames, attributeNames, tableName);
        }
    }

    private ArrayList<String> getValueNameList(String tempValueNames) {
        ArrayList<String> valueNames = new ArrayList<>(Arrays.asList(tempValueNames.split(",")));
        for (int i = 0; i < valueNames.size(); i++) {
            String value = valueNames.get(i);
            if (value.startsWith("'") && value.endsWith("'")) {
                valueNames.set(i, value.substring(1, value.length() - 1));
            }
        }
        return valueNames;
    }


    public void insertValuesIfValid(String tableName, ArrayList<String> attributeNames, String tempValueNames) {
        int primaryKeyIndex = getPrimaryKeyIndexFromArray(tableName, attributeNames);
        ArrayList<String> valueNames = getValueNameList(tempValueNames);
        if (attributeNames.size() == valueNames.size()) {
            if (canInsert(attributeNames, valueNames, tableName)) {
                String concatedValues = concatValues(attributeNames, valueNames, tableName, primaryKeyIndex);
                if (concatedValues.equals("ERROR")) {
                    out.println("Error: " + "Not null attributes should be assigned a value");
                } else {
                    String mongoKey = valueNames.get(primaryKeyIndex);
                    boolean oke = keyExists(mongoKey, tableName);

                    Table tempTable = getTable(tableName);
                    for (String attributeName : attributeNames) {
                        if (oke) {
                            break;
                        }
                        Attribute attribute = getAttributeFromTable(attributeName, tableName);
                        Attribute keyAttribute = getAttributeFromTable(attributeNames.get(primaryKeyIndex), tableName);
                        int indexOfAttribute = tempTable.getAttributes().indexOf(attribute);
                        int indexOfKey = tempTable.getAttributes().indexOf(keyAttribute);
                        int usedIndex;
                        if (indexOfAttribute < indexOfKey) {
                            usedIndex = indexOfAttribute;
                        } else {
                            usedIndex = indexOfAttribute - 1;
                        }
                        if (attribute.isUnique()) {
                            oke = uniqueValueExists(concatedValues, tableName, usedIndex);
                        }
                    }

                    if (!oke) {
                        Document document;
                        document = new Document("_id", mongoKey);
                        document = document.append("value", concatedValues); //caninsert
                        database.getCollection(tableName).insertOne(document);
                        int rowAffected = insertIntoIndexesAttrs(mongoKey, valueNames, attributeNames, tableName);
                        rowAffected++;
                        out.println(rowAffected + " rows affected");
                    } else {
                        out.println("Error: " + "Duplicate keys or duplicate unique values");
                    }
                }
            } else {
                out.println("Error: " + "0 rows affected, couldn't insert because of constraint values");
            }
        } else {
            out.println("Error: " + "Invalid input values");
        }
    }

    public String[] getAttrNamesFromTable(String tableName) {

        Table table = getTable(tableName);
        String[] attrNames = new String[table.getAttributes().size()];
        int ind = 0;
        for (Attribute attribute : table.getAttributes()) {
            attrNames[ind++] = attribute.getAttributeName();
        }
        return attrNames;

    }

    public boolean tableExists(String tableName) {
        for (Table temp : currentDatabase.getTables()) {
            if (temp.getTableName().equals(tableName)) {
                return true;
            }
        }
        return false;
    }

    public boolean attributeFromTableExists(String attributeName, String tableName) {
        Table tempTable = getTable(tableName);

        for (Attribute tempAttribute : tempTable.getAttributes()) {
            if (tempAttribute.getAttributeName().equals(attributeName)) {
                return true;
            }
        }
        return false;
    }

    Table getTable(String tableName) {
        for (Table temp : currentDatabase.getTables()) {
            if (temp.getTableName().equals(tableName)) {
                return temp;
            }
        }
        return new Table(null, null);
    }

    public boolean attributeInTable(String tableName, ArrayList<String> attributeNames) {
        int attributeIndex = 0;
        int attributeSize = attributeNames.size();
        Table tempTable = getTable(tableName);
        for (Attribute tempAttribute : tempTable.getAttributes()) {
            if (attributeIndex != attributeSize && tempAttribute.getAttributeName().equals(attributeNames.get(attributeIndex))) {
                attributeIndex++;
            }
        }
        return attributeIndex == attributeNames.size();
    }

    public boolean duplicateExists(ArrayList<String> arrayList) {
        for (int i = 0; i < arrayList.size() - 1; i++) {
            for (int j = i + 1; j < arrayList.size(); j++) {
                if (arrayList.get(i).equals(arrayList.get(j))) {
                    return true;
                }
            }
        }
        return false;
    }

    public int getPrimaryKeyIndexFromArray(String tableName, ArrayList<String> attributeNames) {
        Table tempTable = getTable(tableName);
        for (String attributeName : attributeNames) {
            for (Attribute tempAttribute : tempTable.getAttributes()) {
                if (tempAttribute.getAttributeName().equals(attributeName)) {
                    if (tempAttribute.isPrimaryKey()) {
                        return attributeNames.indexOf(attributeName);
                    }
                    break;
                }
            }
        }
        return -1;
    }

    public int getPrimaryKeyIndexFromTable(String tableName) {
        Table tempTable = getTable(tableName);
        for (Attribute tempAttribute : tempTable.getAttributes()) {
            if (tempAttribute.isPrimaryKey()) {
                return tempTable.getAttributes().indexOf(tempAttribute);
            }
        }
        return -1;
    }

    public int getNonPrimaryAttributeIndex(String tableName, String AttributeName) {
        Table tempTable = getTable(tableName);
        int primaryKeyIndex = getPrimaryKeyIndexFromTable(tableName);
        for (Attribute tempAttribute : tempTable.getAttributes()) {
            if (tempAttribute.getAttributeName().equals(AttributeName)) {
                int attributeIndex = tempTable.getAttributes().indexOf(tempAttribute);
                if (attributeIndex < primaryKeyIndex) {
                    return attributeIndex;
                }
                return attributeIndex - 1;
            }
        }
        return -1;
    }

    String concatValues(ArrayList<String> attributeNames, ArrayList<String> valueNames, String tableName,
                        int primaryKeyIndex) {
        StringBuilder concatValues = new StringBuilder();
        int attributeIndex = 0;
        int attributeSize = attributeNames.size();
        Table tempTable = getTable(tableName);
        for (Attribute tempAttribute : tempTable.getAttributes()) {
            if (attributeIndex != attributeSize && tempAttribute.getAttributeName().equals(attributeNames.get(attributeIndex))) {
                if (!tempAttribute.getAttributeName().equals(attributeNames.get(primaryKeyIndex))) {
                    concatValues.append(valueNames.get(attributeIndex)).append("#");
                }
                attributeIndex++;
            } else {
                if (!tempAttribute.isNull()) {
                    concatValues.append("null#");
                } else {
                    return "ERROR";
                }
            }
        }

        String concatValuesFinal = concatValues.toString();
        if (concatValuesFinal.length() != 0) {
            concatValuesFinal = concatValuesFinal.substring(0, concatValuesFinal.length() - 1);
        } else {
            concatValuesFinal = "";
        }
        return concatValuesFinal;
    }

    boolean keyExists(String valueName, String tableName) {
        MongoCollection<Document> mongoCollection = database.getCollection(tableName);
        for (Document document : mongoCollection.find()) {
            if (document.get("_id").equals(valueName)) {
                return true;
            }
        }
        return false;
    }

    Document getKeyValueFromMongo(String valueName, String tableName) {
        MongoCollection<Document> mongoCollection = database.getCollection(tableName);
        for (Document document : mongoCollection.find()) {
            if (document.get("_id").equals(valueName)) {
                return document;
            }
        }
        return null;
    }

    boolean uniqueValueExists(String concatedValues, String tableName, int indexInMongo) {
        String valueName = concatedValues.split("#")[indexInMongo];
        boolean oke = false;
        MongoCollection<Document> mongoCollection = database.getCollection(tableName);
        MongoCursor<Document> mongoCursor = mongoCollection.find().iterator();
        while (!oke && mongoCursor.hasNext()) {
            Document document = mongoCursor.next();
            String[] values = document.get("value").toString().split("#");
            if (values[indexInMongo].equals(valueName)) {
                oke = true;
            }
        }
        return oke;
    }

    public Attribute getAttributeFromTable(String attributeName, String tableName) {
        Table tempTable = getTable(tableName);
        for (Attribute tempAttribute : tempTable.getAttributes()) {
            if (tempAttribute.getAttributeName().equals(attributeName))
                return tempAttribute;
        }
        return new Attribute(null, null, false, false, false, null, null);
    }

    public boolean miniExpressionTrue(String type, String value, String expression, String compared) {

        if (type.equalsIgnoreCase("int")) {
            switch (expression) {
                case "=" -> {
                    return Integer.parseInt(value) == Integer.parseInt(compared);
                }
                case "<" -> {
                    return Integer.parseInt(value) < Integer.parseInt(compared);
                }
                case ">" -> {
                    return Integer.parseInt(value) > Integer.parseInt(compared);
                }
                case "<=" -> {
                    return Integer.parseInt(value) <= Integer.parseInt(compared);
                }
                case ">=" -> {
                    return Integer.parseInt(value) >= Integer.parseInt(compared);
                }
                case "!=" -> {
                    return Integer.parseInt(value) != Integer.parseInt(compared);
                }
                default -> {
                    return false;
                }
            }
        } else if (type.equalsIgnoreCase("float")) {
            switch (expression) {
                case "=" -> {
                    return Float.parseFloat(value) == Float.parseFloat(compared);
                }
                case "<" -> {
                    //return value.compareTo(compared) < 0;
                    return Float.parseFloat(value) < Float.parseFloat(compared);
                }
                case ">" -> {
                    return Float.parseFloat(value) > Float.parseFloat(compared);
                }
                case "<=" -> {
                    return Float.parseFloat(value) <= Float.parseFloat(compared);
                }
                case ">=" -> {
                    return Float.parseFloat(value) >= Float.parseFloat(compared);
                }
                case "!=" -> {
                    return Float.parseFloat(value) != Float.parseFloat(compared);
                }
                default -> {
                    return false;
                }
            }
        } else if (type.startsWith("VARCHAR")) {
            switch (expression) {
                case "=" -> {
                    return value.equals(compared);
                }
                case "<" -> {
                    return value.compareTo(compared) < 0;
                }
                case ">" -> {
                    return value.compareTo(compared) > 0;
                }
                case "<=" -> {
                    return value.compareTo(compared) <= 0;
                }
                case ">=" -> {
                    return value.compareTo(compared) >= 0;
                }
                case "!=" -> {
                    return !value.equals(compared);
                }
                default -> {
                    return false;
                }
            }
        }

        return false;
    }

    public boolean expressionTrue(Document document, String tableName, String attributeName, String
            expression, String compared) {
        Attribute tempAttribute = getAttributeFromTable(attributeName, tableName);
        if (tempAttribute.isPrimaryKey()) {
            String key = document.get("_id").toString();
            return miniExpressionTrue(tempAttribute.getType(), key, expression, compared);
        } else {
            String attributes = document.get("value").toString();
            int attributeIndex = getNonPrimaryAttributeIndex(tableName, attributeName);
            String attributeValue = attributes.split("#")[attributeIndex];
            return miniExpressionTrue(tempAttribute.getType(), attributeValue, expression, compared);
        }
    }

    public boolean correspondsToExpressions(Document document, String[] utasitasok, String tableName) {
        ArrayList<Boolean> andBooleans = new ArrayList<>();
        int currentAttr = 4;
        boolean currentExpression = true;
        while (currentAttr < utasitasok.length) {
            if (!expressionTrue(document, tableName, utasitasok[currentAttr], utasitasok[currentAttr + 1], utasitasok[currentAttr + 2])) {
                currentExpression = false;
            }

            if (currentAttr + 3 < utasitasok.length) {
                if (utasitasok[currentAttr + 3].toUpperCase().equals("OR")) {
                    andBooleans.add(currentExpression);
                    currentExpression = true;
                }
                currentAttr += 4;
            } else {
                currentAttr += 4;
                andBooleans.add(currentExpression);
                currentExpression = true;
            }
        }
        for (boolean andBoolean : andBooleans) {
            if (andBoolean) {
                return true;
            }
        }
        return false;
    }

    ArrayList<Pair<Integer, Index>> getIndexesOfAttributesAndIndexNames(String tableName) {

        Table table = getTable(tableName);
        ArrayList<Attribute> attributes = table.getAttributes();
        ArrayList<Pair<Integer, Index>> indOfAttrAndIndNames = new ArrayList<>();

        for (Attribute attribute : attributes) {

            if (attribute.isPrimaryKey()) {
                continue;
            }

            for (Index index : currentDatabase.getIndexes()) {

                String attributeName = attribute.getAttributeName();
                if (index.getTableName().equals(tableName) && index.getAttributeName().equals(attributeName)) {

                    int indexOfAttr = getNonPrimaryAttributeIndex(tableName, attributeName);
                    indOfAttrAndIndNames.add(new Pair<>(indexOfAttr, index));
                    break;

                }

            }

        }

        return indOfAttrAndIndNames;
    }

    ArrayList<Pair<String, String>> getAttrNamesAndIndexNames(String tableName) {

        Table table = getTable(tableName);
        ArrayList<Attribute> attributes = table.getAttributes();
        ArrayList<Pair<String, String>> attrNamesAndIndNames = new ArrayList<>();

        for (Attribute attribute : attributes) {

            if (attribute.isPrimaryKey()) {
                continue;
            }

            for (Index index : currentDatabase.getIndexes()) {

                String attributeName = attribute.getAttributeName();
                if (index.getTableName().equals(tableName) && index.getAttributeName().equals(attributeName)) {

                    attrNamesAndIndNames.add(new Pair<>(attributeName, index.getIndexName()));
                    break;

                }

            }

        }

        return attrNamesAndIndNames;
    }

    ArrayList<Tuple<String, String, String>> getReferencingTablesAndAttrs(String tableName) {

        ArrayList<Tuple<String, String, String>> referencingTableAndAttr = new ArrayList<>();

        for (Table table : currentDatabase.getTables()) {
            for (Attribute attribute : table.getAttributes()) {
                if (attribute.getForeignTable().equals(tableName)) {
                    referencingTableAndAttr.add(new Tuple<>(table.getTableName(), attribute.getAttributeName(), attribute.getForeignAttribute()));
                }
            }
        }

        return referencingTableAndAttr;

    }

    boolean containsValue(String tableName, String attrName, String value) {

        Attribute attribute = getAttributeFromTable(attrName, tableName);
        System.out.println(attribute.isPrimaryKey());
        int foreignIndex = getNonPrimaryAttributeIndex(tableName, attrName);

        MongoCollection<Document> mongoCollection = database.getCollection(tableName);
        for (Object o : mongoCollection.find()) {
            Document foreignDocument = (Document) o;

            String foreignValue = "";
            if (attribute.isPrimaryKey()) {
                foreignValue = foreignDocument.get("_id").toString();
            } else {
                foreignValue = foreignDocument.get("value").toString().split("#")[foreignIndex];
            }

            if (foreignValue.equals(value)) {
                return true;
            }

        }

        return false;

    }

    boolean canInsert(ArrayList<String> attributeNames, ArrayList<String> valueNames, String tableName) {

        for (String attributeName : attributeNames) {
            Attribute attribute = getAttributeFromTable(attributeName, tableName);
            if (!attribute.getForeignTable().equals("")) {
                String foreignTable = attribute.getForeignTable();
                String foreignAttrName = attribute.getForeignAttribute();
                String value = valueNames.get(attributeNames.indexOf(attributeName));
                if (!containsValue(foreignTable, foreignAttrName, value)) {
                    return false;
                }
            }
        }

        return true;

    }

    boolean canDelete(ArrayList<Tuple<String, String, String>> referencingTableAndAttrs, Document
            document, String tableName) {

        for (Tuple<String, String, String> referencingTableAndAttr : referencingTableAndAttrs) {

            String foreignTableName = referencingTableAndAttr.getFirst();
            String foreignAttrName = referencingTableAndAttr.getSecond();
            String attrName = referencingTableAndAttr.getThird();
            Attribute attribute = getAttributeFromTable(attrName, tableName);
            Attribute foreignAttribute = getAttributeFromTable(foreignAttrName, foreignTableName);

            String valueOfAttr = "";
            if (attribute.isPrimaryKey()) {
                valueOfAttr = document.get("_id").toString();
            } else {
                int indexOfAttr = getNonPrimaryAttributeIndex(tableName, attrName);
                valueOfAttr = document.get("value").toString().split("#")[indexOfAttr];
            }

            int foreignIndex = getNonPrimaryAttributeIndex(foreignTableName, foreignAttrName);

            MongoCollection<Document> mongoCollection = database.getCollection(foreignTableName);
            for (Object o : mongoCollection.find()) {
                Document foreignDocument = (Document) o;

                String foreignValue = "";
                if (foreignAttribute.isPrimaryKey()) {
                    foreignValue = foreignDocument.get("_id").toString();
                } else {
                    foreignValue = foreignDocument.get("value").toString().split("#")[foreignIndex];
                }

                if (foreignValue.equals(valueOfAttr)) {
                    return false;
                }

            }

        }

        return true;
    }

    public int deleteFromTable(String[] utasitasok, String tableName) {
        int rowsAffected = 0;

        MongoCollection<Document> mongoCollection = database.getCollection(tableName);

        ArrayList<Pair<Integer, Index>> indOfAttrsAndIndNames = getIndexesOfAttributesAndIndexNames(tableName);
        ArrayList<Tuple<String, String, String>> referencingTableAndAttr = getReferencingTablesAndAttrs(tableName);

        for (Document o : mongoCollection.find()) {

            if (canDelete(referencingTableAndAttr, o, tableName)) {

                if (correspondsToExpressions(o, utasitasok, tableName)) {
                    mongoCollection.deleteOne(o);

                    String primaryKey = o.get("_id").toString();
                    String value = o.get("value").toString();

                    rowsAffected = rowsAffected + deleteFromIndex(indOfAttrsAndIndNames, primaryKey, value);
                    rowsAffected++;
                }
            }
        }
        return rowsAffected;
    }

    public int deleteFromIndex(ArrayList<Pair<Integer, Index>> indOfAttrsAndIndexes, String primaryKey, String
            value) {

        String[] attrValues = value.split("#");

        for (Pair<Integer, Index> indOfAttrAndInd : indOfAttrsAndIndexes) {
            int indOfAttr = indOfAttrAndInd.getFirst();
            String indexName = indOfAttrAndInd.getSecond().getIndexName();
            String valueOfAttr = attrValues[indOfAttr];
            Document document = getKeyValueFromMongo(valueOfAttr, indexName);
            String concatedValue = document.get("value").toString();

            if (concatedValue.equals(primaryKey)) {
                database.getCollection(indexName).deleteOne(document);
            } else {
                String[] tempConcated = concatedValue.split("#");
                StringBuilder updatedValue = new StringBuilder();
                for (String tempConcat : tempConcated) {
                    if (!tempConcat.equals(primaryKey)) {
                        updatedValue.append(tempConcat).append("#");
                    }
                }
                updatedValue.deleteCharAt(updatedValue.length() - 1);
                Document updatedDocument = new Document("_id", valueOfAttr);
                updatedDocument.append("value", updatedValue.toString());
                database.getCollection(indexName).replaceOne(new BasicDBObject("_id", valueOfAttr), updatedDocument, new UpdateOptions().upsert(true));
            }


        }

        return indOfAttrsAndIndexes.size();

    }

    public boolean indexOnAttributeExists(String attributeName, String tableName, String indexName) {

        for (Index tempIndex : currentDatabase.getIndexes()) {
            if (tempIndex.getAttributeName().equals(attributeName) && tempIndex.getTableName().equals(tableName)) {
                return true;
            }
        }

        return false;
    }

    public void insertNewIndexInMongo(String indexName, String attributeName, String tableName) {
        Attribute attribute = getAttributeFromTable(attributeName, tableName);
        if (attribute.isUnique()) {
            int rowsAffected = insertNewIndexInMongoUniqueAttr(indexName, attributeName, tableName);
            out.println(rowsAffected + " rows affected");
        } else {
            int rowsAffected = insertNewIndexInMongoNotUniqueAttr(indexName, attributeName, tableName);
            out.println(rowsAffected + " rows affected");
        }
    }

    public int insertNewIndexInMongoUniqueAttr(String indexName, String attributeName, String tableName) {
        int rowsAffected = 0;
        int attributeIndex = getNonPrimaryAttributeIndex(tableName, attributeName);
        MongoCollection<Document> mongoCollection = database.getCollection(tableName);
        for (Document o : mongoCollection.find()) {
            Document document = o;
            String primaryKeyValue = document.get("_id").toString();
            String uniqueValue = document.get("value").toString().split("#")[attributeIndex];
            document = new Document("_id", uniqueValue);
            document = document.append("value", primaryKeyValue);
            database.getCollection(indexName).insertOne(document);
            rowsAffected++;
        }
        return rowsAffected;
    }

    public int insertNewIndexInMongoNotUniqueAttr(String indexName, String attributeName, String tableName) {
        HashMap<String, String> attrConcatPrimary = new HashMap<>();

        int rowsAffected = 0;
        int attributeIndex = getNonPrimaryAttributeIndex(tableName, attributeName);
        MongoCollection<Document> mongoCollection = database.getCollection(tableName);

        for (Document o : mongoCollection.find()) {
            Document document = o;
            String primaryKeyValue = document.get("_id").toString();
            String nonUniqueValue = document.get("value").toString().split("#")[attributeIndex];
            if (attrConcatPrimary.containsKey(nonUniqueValue)) {
                String tempValue = attrConcatPrimary.get(nonUniqueValue) + "#" + primaryKeyValue;
                attrConcatPrimary.put(nonUniqueValue, tempValue);
            } else {
                attrConcatPrimary.put(nonUniqueValue, primaryKeyValue);
            }

        }

        for (HashMap.Entry<String, String> entry : attrConcatPrimary.entrySet()) {
            String key = entry.getKey();
            String value = entry.getValue();
            Document document = new Document("_id", key);
            document = document.append("value", value);
            database.getCollection(indexName).insertOne(document);
            rowsAffected++;
        }

        return rowsAffected;

    }

    public void insertUniqueAttrIntoIndex(String primaryKey, String uniqueValue, Index index) {
        Document document = new Document("_id", uniqueValue);
        document = document.append("value", primaryKey);
        database.getCollection(index.getIndexName()).insertOne(document);
    }

    public void insertNonUniqueAttrIntoIndex(String primaryKey, String nonUniqueValue, Index index) {

        String indexName = index.getIndexName();

        if (!keyExists(nonUniqueValue, indexName)) {
            Document document = new Document("_id", nonUniqueValue);
            document = document.append("value", primaryKey);
            database.getCollection(indexName).insertOne(document);
        } else {
            Document document = getKeyValueFromMongo(nonUniqueValue, indexName);
            String updatedValue = document.get("value").toString() + "#" + primaryKey;
            Document updatedDocument = new Document("_id", nonUniqueValue);
            updatedDocument.append("value", updatedValue);
            database.getCollection(indexName).replaceOne(new BasicDBObject("_id", nonUniqueValue), updatedDocument, new UpdateOptions().upsert(true));
        }
    }


    public int insertIntoIndexesAttrs(String primaryKey, ArrayList<String> valueNames, ArrayList<String> attributeNames, String tableName) {

        int rowsAffected = 0;

        for (String attributeName : attributeNames) {

            for (Index index : currentDatabase.getIndexes()) {

                if (index.getTableName().equals(tableName) && index.getAttributeName().equals(attributeName)) {

                    Attribute attribute = getAttributeFromTable(attributeName, tableName);
                    int indexOfAttr = attributeNames.indexOf(attributeName);

                    String attrValue = valueNames.get(indexOfAttr);

                    if (!attribute.isPrimaryKey()) {
                        if (attribute.isUnique()) {
                            insertUniqueAttrIntoIndex(primaryKey, attrValue, index);
                        } else {
                            insertNonUniqueAttrIntoIndex(primaryKey, attrValue, index);
                        }
                        rowsAffected++;
                    }

                }

            }

        }

        return rowsAffected;
    }

    public ArrayList<String> selectFromTable(String tableName, String[] criteriums) {

        ArrayList<Pair<String, String>> attrNamesFromTableWithIndexNames = getAttrNamesAndIndexNames(tableName);
        ArrayList<Integer> attrIndsWithoutIndexes = new ArrayList<>();
        ArrayList<String> primaryKeys = new ArrayList<>();
        boolean isFirst = true;
        for (int i = 5; i < criteriums.length; i += 4) {

            String attrName = criteriums[i];
            boolean hasIndex = false;
            String indexName = "";
            for (Pair<String, String> attrNameIndName : attrNamesFromTableWithIndexNames) {
                if (attrNameIndName.getFirst().equals(attrName)) {
                    hasIndex = true;
                    indexName = attrNameIndName.getSecond();
                    break;
                }
            }
            if (hasIndex) {
                MongoCollection<Document> mongoCollection = database.getCollection(indexName);
                ArrayList<String> tempPrimaryKeys = getPrimaryKeysFromCriterium(mongoCollection, criteriums[i + 1], criteriums[i + 2]);
                if (isFirst) {
                    primaryKeys = tempPrimaryKeys;
                    Collections.sort(primaryKeys);
                    isFirst = false;
                } else {
                    Collections.sort(tempPrimaryKeys);
                    primaryKeys = intersectTwoArrays(primaryKeys, tempPrimaryKeys);
                }
            } else {
                attrIndsWithoutIndexes.add(i);
            }

        }
        MongoCollection<Document> mongoCollection = database.getCollection(tableName);
        if (isFirst) {
            for (Document document : mongoCollection.find()) {
                primaryKeys.add(document.get("_id").toString());
            }
        }
        for (int indexes : attrIndsWithoutIndexes) {
            String attrName = criteriums[indexes];
            ArrayList<String> tempPrimaryKeys = new ArrayList<>();
            Attribute attribute = getAttributeFromTable(attrName, tableName);

            if (attribute.isPrimaryKey()) {
                for (String primaryKey : primaryKeys) {
                    if (miniExpressionTrue(attribute.getType(), primaryKey, criteriums[indexes + 1], criteriums[indexes + 2])) {
                        tempPrimaryKeys.add(primaryKey);
                    }
                }
            } else {
                int indexOfAttr = getNonPrimaryAttributeIndex(tableName, attrName);
                for (String primaryKey : primaryKeys) {
                    String values = Objects.requireNonNull(mongoCollection.find(Filters.eq("_id", primaryKey)).first()).get("value").toString();
                    String value = values.split("#")[indexOfAttr];
                    if (miniExpressionTrue(attribute.getType(), value, criteriums[indexes + 1], criteriums[indexes + 2])) {
                        tempPrimaryKeys.add(primaryKey);
                    }
                }
            }
            primaryKeys = tempPrimaryKeys;
        }

        return primaryKeys;
    }

    public ArrayList<String> getPrimaryKeysFromCriterium(MongoCollection<Document> mongoCollection, String criterium1, String criterium2) {
        ArrayList<String> primaryKeys = new ArrayList<>();
        Bson bsonFilter = switch (criterium1) {
            case "=" -> Filters.eq("_id", criterium2);
            case ">" -> Filters.gt("_id", criterium2);
            case "<" -> Filters.lt("_id", criterium2);
            case "<=" -> Filters.lte("_id", criterium2);
            case ">=" -> Filters.gte("_id", criterium2);
            case "!=" -> Filters.ne("_id", criterium2);
            default -> throw new IllegalStateException("Unexpected value: " + criterium1);
        };

        for (Document document : mongoCollection.find(bsonFilter)) {
            String[] keyNames = document.get("value").toString().split("#");
            Collections.addAll(primaryKeys, keyNames);
        }

        return primaryKeys;
    }

    public ArrayList<String> intersectTwoArrays(ArrayList<String> array1, ArrayList<String> array2) {
        ArrayList<String> intersectedArray = new ArrayList<>();
        for (String string : array1) {
            if (array2.contains(string)) {
                intersectedArray.add(string);
            }
        }

        return intersectedArray;
    }

    public ArrayList<Integer> getAttrNamesIndexes(String tableName, String[] attrNames) {

        ArrayList<Integer> attrNamesIndexes = new ArrayList<>();
        for (String attrName : attrNames) {
            Attribute attribute = getAttributeFromTable(attrName, tableName);
            if (attribute.isPrimaryKey()) {
                attrNamesIndexes.add(-1);
            } else {
                attrNamesIndexes.add(getNonPrimaryAttributeIndex(tableName, attrName));
            }

        }

        return attrNamesIndexes;
    }

    public void writeoutSelectedAttrs(ArrayList<String> primaryKeys, String
            tableName, ArrayList<Integer> attrNamesIndexes, String[] attrNames) {
        MongoCollection<Document> mongoCollection = database.getCollection(tableName);
        Bson bsonFilter = Filters.in("_id", primaryKeys);
        for (String attrName : attrNames) {
            out.print(attrName + " ");
        }
        out.print("\n");
        for (Document document : mongoCollection.find(bsonFilter)) {
            writeoutDocumentValuesByAttrIndexes(attrNamesIndexes, document);
        }
        out.println("Select command done");
    }

    public void writeoutAllSelectedAttrs(String tableName, ArrayList<Integer> attrNamesIndexes, String[] attrNames) {
        MongoCollection<Document> mongoCollection = database.getCollection(tableName);
        for (String attrName : attrNames) {
            out.print(attrName + " ");
        }
        out.print("\n");
        for (Document document : mongoCollection.find()) {
            writeoutDocumentValuesByAttrIndexes(attrNamesIndexes, document);
        }
        out.println("Select command done");
    }

    private void writeoutDocumentValuesByAttrIndexes(ArrayList<Integer> attrNamesIndexes, Document document) {
        String key = document.get("_id").toString();
        String[] values = document.get("value").toString().split("#");
        for (Integer index : attrNamesIndexes) {
            if (index == -1) {
                out.print(key + " ");
            } else {
                out.print(values[index] + " ");
            }
        }
        out.print("\n");
    }

    ArrayList<Pair<Pair<String, Integer>, Pair<String, Integer>>> getGroupByContents(ArrayList<Pair<String, String>> tableNamePairs, String[] utasitasok) {
        ArrayList<Pair<Pair<String, Integer>, Pair<String, Integer>>> groupByContents = new ArrayList<>();
        int index = 0;
        while (index < utasitasok.length && !(utasitasok[index].equalsIgnoreCase("GROUP") && utasitasok[index + 1].equalsIgnoreCase("BY"))) {
            index++;
        }
        if (index != utasitasok.length) {
            index += 2;
            String[] groupByList = utasitasok[index].split(",");
            String tableAbbr = "";
            String attrName = "";
            for (String tableAttr : groupByList) {
                tableAbbr = tableAttr.split("\\.")[0];
                attrName = tableAttr.split("\\.")[1];
                String longTableName = getTableNameAndIndFromPair(tableAbbr, tableNamePairs).first;
                if (!attributeFromTableExists(attrName, longTableName)) {
                    out.println("Error: " + attrName + " from table " + longTableName + " doesn't exist");
                    return null;
                }
                getLongTableNameAndAttr(tableNamePairs, groupByContents, tableAbbr, attrName);
            }
        }

        return groupByContents;
    }

    private void getLongTableNameAndAttr(ArrayList<Pair<String, String>> tableNamePairs, ArrayList<Pair<Pair<String, Integer>, Pair<String, Integer>>> groupByContents, String tableAbbr, String attrName) {
        Pair<String, Integer> tableNameAndInd = getTableNameAndIndFromPair(tableAbbr, tableNamePairs);

        int attrInd = -1;
        if (!getAttributeFromTable(attrName, tableNameAndInd.first).isPrimaryKey()) {
            attrInd = getNonPrimaryAttributeIndex(tableNameAndInd.first, attrName);
        }
        groupByContents.add(new Pair<>(new Pair<>(tableNameAndInd.first, tableNameAndInd.second), new Pair<>(attrName, attrInd)));
    }

    ArrayList<ArrayList<String>> innerJoin(ArrayList<Pair<String, String>> tableNamePairs, String[] utasitasok) {
        ArrayList<ArrayList<String>> primaryKeys = new ArrayList<>();

        ArrayList<Tuple<Pair<String, String>, String, String>> whereCrit = new ArrayList<>();

        for (int i = 0; i < utasitasok.length; i++) {
            if (utasitasok[i].equalsIgnoreCase("WHERE")) {
                while (i < utasitasok.length && !(utasitasok[i].equalsIgnoreCase("GROUP") && utasitasok[i + 1].equalsIgnoreCase("BY"))) {
                    String[] colWithTable = utasitasok[i + 1].split("\\.");
                    String tableName = getTableNameAndIndFromPair(colWithTable[0], tableNamePairs).first;
                    whereCrit.add(new Tuple<>(new Pair<>(tableName, colWithTable[1]), utasitasok[i + 2], utasitasok[i + 3]));
                    i += 4;
                }
            }
        }

        ArrayList<String> tempPrimaryKeys = getAllKeysWithCriterium(tableNamePairs.get(0), whereCrit);

        if (tempPrimaryKeys.isEmpty()) {
            return primaryKeys;
        }

        for (int i = 0; i < tempPrimaryKeys.size(); i++) {
            primaryKeys.add(new ArrayList<>());
            primaryKeys.get(i).add(tempPrimaryKeys.get(i));
            for (int j = 1; j < tableNamePairs.size(); j++) {
                primaryKeys.get(i).add("EMPTY");
            }
        }

        for (int i = 0; i < utasitasok.length; i++) {
            if (utasitasok[i].equalsIgnoreCase("ON")) {
                String[] firstTable = utasitasok[i + 1].split("\\.");
                String[] secondTable = utasitasok[i + 3].split("\\.");
                Pair<String, Integer> fstTableNameAndInd = getTableNameAndIndFromPair(firstTable[0], tableNamePairs);
                Pair<String, Integer> secondTableNameAndInd = getTableNameAndIndFromPair(secondTable[0], tableNamePairs);
                String firstAttr = firstTable[1];
                String secondAttr = secondTable[1];
                if (primaryKeys.get(0).get(fstTableNameAndInd.second).equals("EMPTY")) {
                    Pair<String, Integer> temp = secondTableNameAndInd;
                    secondTableNameAndInd = fstTableNameAndInd;
                    fstTableNameAndInd = temp;
                    String tempAttr = secondAttr;
                    secondAttr = firstAttr;
                    firstAttr = tempAttr;
                }
                ArrayList<Tuple<String, String, String>> attrCrits = new ArrayList<>();
                for (Tuple<Pair<String, String>, String, String> crits : whereCrit) {
                    if (crits.first.first.equals(secondTableNameAndInd.first)) {
                        attrCrits.add(new Tuple<>(crits.first.second, crits.second, crits.third));
                    }
                }

                primaryKeys = filterResultsInJoin(primaryKeys, fstTableNameAndInd, secondTableNameAndInd, firstAttr, secondAttr, attrCrits);

            }
        }

        return primaryKeys;
    }

    ArrayList<String> getAllKeysWithCriterium(Pair<String, String> tablePair, ArrayList<Tuple<Pair<String, String>, String, String>> whereCrit) {
        ArrayList<Tuple<String, String, String>> attrCrits = new ArrayList<>();
        for (Tuple<Pair<String, String>, String, String> crits : whereCrit) {
            if (crits.first.first.equals(tablePair.first)) {
                attrCrits.add(new Tuple<>(crits.first.second, crits.second, crits.third));
            }
        }
        ArrayList<String> allKeys = new ArrayList<>();
        MongoCollection<Document> mongoCollection = database.getCollection(tablePair.first);
        for (Document document : mongoCollection.find()) {
            String pk = document.get("_id").toString();

            boolean correspondsToCrit = true;

            for (Tuple<String, String, String> attrCrit : attrCrits) {
                if (!expressionTrue(document, tablePair.first, attrCrit.first, attrCrit.second, attrCrit.third)) {
                    correspondsToCrit = false;
                    break;
                }
            }

            if (correspondsToCrit) {
                allKeys.add(pk);
            }
        }
        return allKeys;
    }

    Pair<String, Integer> getTableNameAndIndFromPair(String tableAbbr, ArrayList<Pair<String, String>> tableNamePairs) {
        for (int i = 0; i < tableNamePairs.size(); i++) {
            if (tableNamePairs.get(i).second.equals(tableAbbr)) {
                return new Pair<>(tableNamePairs.get(i).first, i);
            }
        }
        return null;
    }

    public ArrayList<ArrayList<String>> filterResultsInJoin(ArrayList<ArrayList<String>> primaryKeys, Pair<String, Integer> fstTableNameAndInd, Pair<String, Integer> secondTableNameAndInd, String fstAttr, String secondAttr, ArrayList<Tuple<String, String, String>> attrCrits) {
        ArrayList<ArrayList<String>> tempPrimaryKeys = new ArrayList<>();

        int i = 0;

        for (ArrayList<String> primaryKeyList : primaryKeys) {
            ArrayList<String> primaryKeysFromSecond = getPrimaryFromJoin(primaryKeyList.get(fstTableNameAndInd.second), fstTableNameAndInd.first, secondTableNameAndInd.first, fstAttr, secondAttr, attrCrits);
            for (String primaryKeyFromSnd : primaryKeysFromSecond) {
                primaryKeyList.set(secondTableNameAndInd.second, primaryKeyFromSnd);
                tempPrimaryKeys.add(primaryKeyList);
            }

        }

        return tempPrimaryKeys;
    }

    public ArrayList<String> getPrimaryFromJoin(String primaryKey, String fstTableName, String sndTableName, String fstAttr, String sndAttr, ArrayList<Tuple<String, String, String>> attrCrits) {

        Attribute fstAttribute = getAttributeFromTable(fstAttr, fstTableName);
        Attribute sndAttribute = getAttributeFromTable(sndAttr, sndTableName);
        ArrayList<String> primaryKeysFromSecond = new ArrayList<>();

        String fstAttrValue = "";

        if (fstAttribute.isPrimaryKey()) {
            fstAttrValue = primaryKey;
        } else {
            MongoCollection<Document> mongoCollection = database.getCollection(fstTableName);
            for (Document document : mongoCollection.find()) {
                String pk = document.get("_id").toString();
                if (pk.equals(primaryKey)) {
                    int fstAttrIndex = getNonPrimaryAttributeIndex(fstTableName, fstAttr);
                    fstAttrValue = document.get("value").toString().split("#")[fstAttrIndex];
                    break;
                }
            }
        }

        if (sndAttribute.isPrimaryKey()) {
            MongoCollection<Document> mongoCollection = database.getCollection(sndTableName);
            for (Document document : mongoCollection.find()) {
                String pk = document.get("_id").toString();
                if (pk.equals(fstAttrValue)) {
                    boolean correspondsToCrit = true;
                    addToListIfCorresponds(sndTableName, attrCrits, primaryKeysFromSecond, document, pk, correspondsToCrit);
                    break;
                }
            }
        } else {
            ArrayList<Pair<String, String>> attrNamesAndIndexNames = getAttrNamesAndIndexNames(sndTableName);

            boolean hasIndex = false;
            String indexName = "";

            for (Pair<String, String> attrNameAndIndName : attrNamesAndIndexNames) {
                if (attrNameAndIndName.first.equals(sndAttr)) {
                    hasIndex = true;
                    indexName = attrNameAndIndName.second;
                    break;
                }
            }
            if (hasIndex) {
                MongoCollection<Document> mongoCollection = database.getCollection(indexName);
                for (Document document : mongoCollection.find()) {
                    String value = document.get("_id").toString();
                    if (value.equals(fstAttrValue)) {
                        String[] pks = document.get("value").toString().split("#");
                        if (!attrCrits.isEmpty()) {
                            MongoCollection<Document> mongoCollection2 = database.getCollection(fstAttr);
                            List<String> pkList = Arrays.asList(pks);
                            for (Document document2 : mongoCollection2.find()) {
                                String pk = document2.get("_id").toString();
                                if (pkList.contains(pk)) {
                                    addToListIfCorresponds(sndTableName, attrCrits, primaryKeysFromSecond, document2, pk, true);
                                }
                            }
                        } else {
                            Collections.addAll(primaryKeysFromSecond, pks);
                        }
                        break;
                    }
                }
            } else {
                int sndAttrInd = getNonPrimaryAttributeIndex(sndTableName, sndAttr);
                MongoCollection<Document> mongoCollection = database.getCollection(sndTableName);
                for (Document document : mongoCollection.find()) {
                    String value = document.get("value").toString().split("#")[sndAttrInd];
                    if (value.equals(fstAttrValue)) {
                        String pk = document.get("_id").toString();
                        addToListIfCorresponds(sndTableName, attrCrits, primaryKeysFromSecond, document, pk, true);
                    }
                }
            }
        }

        return primaryKeysFromSecond;
    }

    private void addToListIfCorresponds(String sndTableName, ArrayList<Tuple<String, String, String>> attrCrits, ArrayList<String> primaryKeysFromSecond, Document document, String pk, boolean correspondsToCrit) {
        for (Tuple<String, String, String> attrCrit : attrCrits) {
            if (!expressionTrue(document, sndTableName, attrCrit.first, attrCrit.second, attrCrit.third)) {
                correspondsToCrit = false;
                break;
            }
        }
        if (correspondsToCrit) {
            primaryKeysFromSecond.add(pk);
        }
    }

    private ArrayList<Pair<Pair<String, Integer>, Pair<String, Integer>>> getTablesAttrsInds(ArrayList<Pair<String, String>> tableNamePairs) {

        ArrayList<Pair<Pair<String, Integer>, Pair<String, Integer>>> tablesAttrsInds = new ArrayList<>();

        for (Pair<String, String> tableNamePair : tableNamePairs) {

            Table table = getTable(tableNamePair.first);
            Pair<String, Integer> tableNameInd = getTableNameAndIndFromPair(tableNamePair.second, tableNamePairs);
            for (Attribute attr : table.getAttributes()) {
                int ind = -1;
                if (!attr.isPrimaryKey()) {
                    ind = getNonPrimaryAttributeIndex(tableNamePair.first, attr.getAttributeName());
                }
                tablesAttrsInds.add(new Pair<>(new Pair<>(tableNameInd.first, tableNameInd.second), new Pair<>(attr.getAttributeName(), ind)));
            }

        }

        return tablesAttrsInds;
    }

    public void writeoutAllSelectedAttrsJoin(ArrayList<ArrayList<String>> primaryKeys, ArrayList<Pair<Pair<String, Integer>, Pair<String, Integer>>> tablesAttrsInds, ArrayList<Pair<Pair<String, Integer>, Pair<String, Integer>>> groupByTablesAttrsInds, ArrayList<Pair<String, Pair<String, String>>> aggrAndColName) {
        ArrayList<Pair<Integer, String>> indexesOfAggr = new ArrayList<>();
        ArrayList<Integer> indexes = new ArrayList<>();
        for (Pair<String, Pair<String, String>> aggrColName : aggrAndColName) {
            for (Pair<Pair<String, Integer>, Pair<String, Integer>> groupByTablesIndAttr : tablesAttrsInds) {
                if (groupByTablesIndAttr.first.first.equals(aggrColName.second.first) && groupByTablesIndAttr.second.first.equals(aggrColName.second.second)) {
                    int index = tablesAttrsInds.indexOf(groupByTablesIndAttr);
                    if (!indexes.contains(index)) {
                        indexes.add(index);
                        indexesOfAggr.add(new Pair<>(index, aggrColName.first));
                        break;
                    }
                }
            }
        }

        for (Pair<Pair<String, Integer>, Pair<String, Integer>> tablesAttrsInd : tablesAttrsInds) {
            boolean aggrAttr = false;
            for (Pair<String, Pair<String, String>> aggrColName : aggrAndColName) {
                if (aggrColName.second.first.equals(tablesAttrsInd.first.first) && aggrColName.second.second.equals(tablesAttrsInd.second.first)) {
                    aggrAttr = true;
                    break;
                }
            }
            if (!aggrAttr) {
                out.print(tablesAttrsInd.first.first + "." + tablesAttrsInd.second.first + " ");
            }
        }

        for (Pair<String, Pair<String, String>> aggrColName : aggrAndColName) {
            out.print(aggrColName.first + "(" + aggrColName.second.first + "." + aggrColName.second.second + ")" + " ");
        }

        out.print("\n");

        ArrayList<ArrayList<String>> values = new ArrayList<>();

        for (ArrayList<String> primaryKeyList : primaryKeys) {
            ArrayList<String> rowValues = new ArrayList<>();
            for (Pair<Pair<String, Integer>, Pair<String, Integer>> tablesAttrsInd : tablesAttrsInds) {
                MongoCollection<Document> mongoCollection = database.getCollection(tablesAttrsInd.first.first);
                for (Document document : mongoCollection.find()) {
                    String key = document.get("_id").toString();
                    if (primaryKeyList.get(tablesAttrsInd.first.second).equals(key)) {
                        String value = getAttrValueFromDocumentByGivenID(tablesAttrsInd.second, document);
                        rowValues.add(value);
                        break;
                    }
                }
            }
            values.add(rowValues);
        }

        ArrayList<ArrayList<String>> newValues = new ArrayList<>();

        for (Pair<Pair<String, Integer>, Pair<String, Integer>> groupByTablesIndAttr : groupByTablesAttrsInds) {
            newValues = new ArrayList<>();
            int index = getIndexOfTableAndAttr(groupByTablesIndAttr, tablesAttrsInds);
            String type = getAttributeFromTable(groupByTablesIndAttr.second.first, groupByTablesIndAttr.first.first).getType();
            customSort(type, values, index);
            int start = 0;

            while (start < values.size()) {
                String value = values.get(start).get(index);
                int start2 = start + 1;
                ArrayList<String> rowValues = values.get(start);

                while (start2 < values.size() && values.get(start2).get(index).equals(value)) {
                    ArrayList<String> blendRows = values.get(start2);
                    blendTwoRows(rowValues, blendRows, indexesOfAggr);
                    start2++;
                }
                calcAvgAndCount(rowValues, indexesOfAggr, start2 - start);
                start = start2;
                newValues.add(rowValues);
            }
            values = newValues;
        }

        if (aggrAndColName.size() == tablesAttrsInds.size()) {
            int start = 0;
            ArrayList<String> rowValues = values.get(start);
            while (start < values.size()) {
                ArrayList<String> blendRows = values.get(start);
                blendTwoRows(rowValues, blendRows, indexesOfAggr);
                start++;
            }
            calcAvgAndCount(rowValues, indexesOfAggr, start);
            newValues.add(rowValues);
        }

        if (!newValues.isEmpty()) {
            values = newValues;
        }
        for (ArrayList<String> valueRow : values) {
            for (String value : valueRow) {
                out.print(value + " ");
            }
            out.print("\n");
        }


    }

    public void calcAvgAndCount(ArrayList<String> rowValues, ArrayList<Pair<Integer, String>> indexesOfAggr, int count) {
        for (Pair<Integer, String> indexOfAggr : indexesOfAggr) {
            int index = indexOfAggr.first;
            float value = Float.parseFloat(rowValues.get(index));
            switch (indexOfAggr.second) {
                case "AVG" -> rowValues.add(index, Float.toString(value / (float) count));
                case "COUNT" -> rowValues.add(index, Integer.toString(count));
            }
        }
    }

    public void blendTwoRows(ArrayList<String> rowValues, ArrayList<String> blendRow, ArrayList<Pair<Integer, String>> indexesOfAggr) {

        for (Pair<Integer, String> indexOfAggr : indexesOfAggr) {
            int index = indexOfAggr.first;
            float value1 = Float.parseFloat(rowValues.get(index));
            float value2 = Float.parseFloat(blendRow.get(index));
            switch (indexOfAggr.second) {
                case "MIN":
                    if (value1 > value2) {
                        rowValues.add(index, Float.toString(value2));
                    }
                    break;
                case "MAX":
                    if (value1 < value2) {
                        rowValues.add(index, Float.toString(value2));
                    }
                    break;
                case "SUM":
                case "AVG":
                    rowValues.add(index, Float.toString(value1 + value2));
                    break;
            }
        }
    }

    public int getIndexOfTableAndAttr(Pair<Pair<String, Integer>, Pair<String, Integer>> groupByTablesIndAttr, ArrayList<Pair<Pair<String, Integer>, Pair<String, Integer>>> tablesAttrsInds) {

        int index;
        for (index = 0; index < tablesAttrsInds.size(); index++) {
            Pair<Pair<String, Integer>, Pair<String, Integer>> tablesAttrsInd = tablesAttrsInds.get(index);
            if (groupByTablesIndAttr.first.first.equals(tablesAttrsInd.first.first) && groupByTablesIndAttr.first.second.equals(tablesAttrsInd.first.second) && groupByTablesIndAttr.second.first.equals(tablesAttrsInd.second.first) && groupByTablesIndAttr.second.second.equals(tablesAttrsInd.second.second)) {
                return index;
            }
        }

        return -1;
    }

    public void customSort(String type, ArrayList<ArrayList<String>> values, int index) {
        if (type.equalsIgnoreCase("INT")) {
            values.sort(Comparator.comparing(o -> Integer.parseInt(o.get(index))));
        } else if (type.startsWith("VARCHAR")) {
            values.sort(Comparator.comparing(o -> o.get(index)));
        } else if (type.equalsIgnoreCase("FLOAT")) {
            values.sort(Comparator.comparing(o -> Float.parseFloat(o.get(index))));
        }
    }

    public String getAttrValueFromDocumentByGivenID(Pair<String, Integer> attrNameIndex, Document document) {

        if (attrNameIndex.second == -1) {
            return document.get("_id").toString();
        } else {
            return document.get("value").toString().split("#")[attrNameIndex.second];
        }

    }

    public boolean allAttrsPresent(ArrayList<Pair<Pair<String, Integer>, Pair<String, Integer>>> tablesAttrsInds,
                                   ArrayList<Pair<String, Pair<String, String>>> aggrAndColNames,
                                   ArrayList<Pair<Pair<String, Integer>, Pair<String, Integer>>> groupByTablesAttrsInds) {

        boolean exists = false;
        for (Pair<Pair<String, Integer>, Pair<String, Integer>> tablesAttrsInd : tablesAttrsInds) {
            String tableName = tablesAttrsInd.first.first;
            String attrName = tablesAttrsInd.second.first;
            for (Pair<String, Pair<String, String>> aggrAndColName : aggrAndColNames) {
                if (aggrAndColName.second.first.equals(tableName) && aggrAndColName.second.second.equals(attrName)) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                for (Pair<Pair<String, Integer>, Pair<String, Integer>> groupByTablesAttrsInd : groupByTablesAttrsInds) {
                    if (groupByTablesAttrsInd.first.first.equals(tableName) && groupByTablesAttrsInd.second.first.equals(attrName)) {
                        exists = true;
                        break;
                    }
                }
            }
            if (!exists) {
                out.println("Error: " + attrName + " from table " + tableName + " doesn't appear in aggragate and group by functions");
                return false;
            }

        }

        for (Pair<Pair<String, Integer>, Pair<String, Integer>> groupByTablesAttrsInd : groupByTablesAttrsInds) {
            String tableName = groupByTablesAttrsInd.first.first;
            String attrName = groupByTablesAttrsInd.second.first;
            exists = false;
            for (Pair<Pair<String, Integer>, Pair<String, Integer>> tablesAttrsInd : tablesAttrsInds) {
                String tableName2 = tablesAttrsInd.first.first;
                String attrName2 = tablesAttrsInd.second.first;
                if (tableName.equals(tableName2) && attrName.equals(attrName2)) {
                    exists = true;
                    break;
                }
            }
            if (!exists) {
                out.println("Error: " + attrName + " from table " + tableName + " isn't present in the SELECT command");
                return false;
            }
            for (Pair<String, Pair<String, String>> aggrAndColName : aggrAndColNames) {
                if (aggrAndColName.second.first.equals(tableName) && aggrAndColName.second.second.equals(attrName)) {
                    out.println("Error: " + attrName + " from table " + tableName + " is already present in the aggr command");
                    return false;
                }
            }
        }

        return true;

    }

    public static void main(String[] args) throws IOException {

        Server server = new Server();
        server.start(6666);

    }

}