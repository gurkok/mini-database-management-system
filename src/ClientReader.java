

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Locale;

public class ClientReader extends Thread{
    private final Socket socket;
    private final ClientFrame frame;
    private BufferedReader in;
    public boolean isQuery = false;
    public boolean runQueryDesigner = false;
    private final Client client;

    public ClientReader(Socket socket, ClientFrame frame, Client client){
        this.client = client;
        this.frame = frame;
        this.socket = socket;
        try {
            in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
        } catch (IOException exception) {
            exception.printStackTrace();
        }
    }

    @Override
    public void run(){
        String out;
        while(!socket.isClosed()){
            try {
                out = in.readLine();
                System.out.println(out);
                if(isQuery){
                    if(!out.toLowerCase(Locale.ROOT).startsWith("error:")) {
                        isQuery = false;
                        String vals = in.readLine();
                        ArrayList<Output> outputs = new ArrayList<>();
                        if(vals.equals("Select command done")){
                            outputs.add(new Output(out));
                        }
                        while (!vals.equals("Select command done")) {
                            System.out.println(vals);
                            outputs.add(new Output(out, vals));
                            vals = in.readLine();
                        }
                        frame.tabbedPane.remove(1);
                        frame.myTable = new MyTable(outputs);
                        frame.tabbedPane.add("TableView", frame.myTable);
                        frame.repaint();

                        frame.textArea.append(vals + "\n");
                    } else {
                        frame.textArea.append(out + "\n");
                    }
                }
                else {
                    if (runQueryDesigner) {
                        runQueryDesigner = false;
                        boolean ok = true;
                        String vals = out;
                        System.out.println(vals);
                        if (vals.equalsIgnoreCase("Please use a database")) {
                            frame.textArea.append(vals + "\n");
                            ok = false;
                        }
                        if (vals.equalsIgnoreCase("Get tables done")) {
                            frame.textArea.append("No tables in the database\n");
                            ok = false;
                        }
                        if (ok) {
                            ArrayList<String> tables = new ArrayList<>();
                            while (!vals.equalsIgnoreCase("Get tables done")) {
                                tables.add(vals);
                                vals = in.readLine();
                            }
                            frame.textArea.append("Running query designer...\n");
                            new QueryDesigner(frame, tables);
                        }
                    } else {
                        frame.textArea.append(out + "\n");
                    }
                }
            } catch (IOException ignored) {
            }
        }
    }
}
