import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

public class QueryDesigner extends JFrame implements ActionListener {

    private final ClientFrame clientFrame;
    private final JButton addTable;
    private final JButton makeQuery;
    private ArrayList<TextField> tables = new ArrayList<>();
    private ArrayList<Label> tableLabel = new ArrayList<>();
    JPanel centerPanel = new JPanel(new GridLayout(10,2,0,0));
    private int tableCount = 0;
    private String[] databaseTables;
    private ArrayList<JComboBox> tableNames;

    public QueryDesigner(ClientFrame clientFrame, ArrayList<String> databaseTables){

        this.databaseTables = new String[databaseTables.size()];
        for(int i=0;i<databaseTables.size();i++){
            this.databaseTables[i] = databaseTables.get(i);
        }

        tableNames = new ArrayList<>();

        this.clientFrame = clientFrame;

        setDefaultCloseOperation(DISPOSE_ON_CLOSE);

        setLayout(new BorderLayout());

        Container cp = getContentPane();

        setTitle("Query Designer");
        setBounds(0, 0, 600, 400);

        JPanel northPanel = new JPanel();
        northPanel.setBorder(new TitledBorder(new EtchedBorder()));

        addTable = new JButton("Add Table");
        addTable.addActionListener(this);
        northPanel.add(addTable);

        makeQuery = new JButton("Generate Query");
        makeQuery.addActionListener(this);
        northPanel.add(makeQuery);

        northPanel.setBounds(25,15,550,40);

        tableNames.add(new JComboBox<>(this.databaseTables));

        tableLabel.add(new Label("table" + ++tableCount + ": "));
        centerPanel.add(tableLabel.get(0));
        centerPanel.add(tableNames.get(0));

        cp.add(northPanel,BorderLayout.NORTH);
        cp.add(centerPanel,BorderLayout.CENTER);

        setVisible(true);


    }

    @Override
    public void actionPerformed(ActionEvent e) {
        if(e.getSource().equals(addTable)){
            if(tableCount<10) {
                tableLabel.add(new Label("table" + ++tableCount + ": "));
                centerPanel.add(tableLabel.get(tableCount - 1));
                tableNames.add(new JComboBox<>(databaseTables));
                centerPanel.add(tableNames.get(tableCount - 1));
                centerPanel.revalidate();
                centerPanel.repaint();
            }
        }
        if(e.getSource().equals(makeQuery)){
            StringBuilder query = new StringBuilder("\nSELECT *");

            query.append("\nFROM ").append(tableNames.get(0).getSelectedItem());

            for(int i=1;i<tableCount;i++){
                query.append("\nINNER JOIN ").append(tableNames.get(i).getSelectedItem()).append("\n").append("ON ...");
            }

            query.append(";\n");

            clientFrame.display.append(query.toString());

            dispose();

        }
    }
}
