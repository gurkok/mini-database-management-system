public class Attribute {
    private final String attributeName;
    private final String type;
    private final boolean isPrimaryKey;
    private final String foreignTable;
    private final String foreignAttribute;
    private final int length;
    private final boolean isNull;
    private final boolean isUnique;


    public Attribute(String attributeName, String type, boolean isNull, boolean isPrimaryKey, boolean isUnique, String foreignTable, String foreignAttribute) {
        this.isPrimaryKey = isPrimaryKey;
        this.attributeName = attributeName;
        this.type = type;
        this.length = 0;
        this.isNull = isNull;
        this.isUnique = isUnique;
        this.foreignTable = foreignTable;
        this.foreignAttribute = foreignAttribute;
    }

    public String getAttributeName() {
        return attributeName;
    }

    public String getType() {
        return type;
    }

    public int getLength() {
        return length;
    }

    public boolean isNull() {
        return isNull;
    }

    public boolean isUnique() {
        return isUnique;
    }

    public boolean isPrimaryKey() {
        return isPrimaryKey;
    }

    public String getForeignTable() {
        return foreignTable;
    }

    public String getForeignAttribute() {
        return foreignAttribute;
    }

    @Override
    public String toString() {
        return "Attribute{" +
                "attributeName='" + attributeName + '\'' +
                ", type='" + type + '\'' +
                ", length=" + length +
                ", isNull=" + isNull +
                ", isUnique=" + isUnique +
                '}';
    }
}
