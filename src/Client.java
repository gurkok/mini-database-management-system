import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.ArrayList;

public class Client {
    private Socket clientSocket;
    public PrintWriter out;
    public BufferedReader in;
    public ArrayList<Output> outputs;

    public void startConnection(String ip, int port) {
        try {
            clientSocket = new Socket(ip, port);
            out = new PrintWriter(clientSocket.getOutputStream(), true);
            in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String sendMessage(String msg) throws IOException {
        if(out!=null)
            out.println(msg);
        if(in!=null)
            return in.readLine();
        return "error";
    }
    public void sendMsg(String msg){
        if(out!=null)
            out.println(msg);
    }

    public void stopConnection() throws IOException {
        if(in!=null)
            in.close();
        if(out!=null)
            out.close();
        if(clientSocket!=null)
            clientSocket.close();
    }

    public static void main(String[] args) throws IOException {
        Client client = new Client();
        client.startConnection("localhost", 6666);

        new ClientFrame(client);
    }

    public Socket getClientSocket() {
        return clientSocket;
    }

}