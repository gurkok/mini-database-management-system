import java.util.ArrayList;

public class Table {

    private final Integer rowLength;
    private final ArrayList<Attribute> attributes;
    private final String tableName;

    public Table(ArrayList<Attribute> attributes, String tableName) {
        this.attributes = attributes;
        this.tableName = tableName;
        rowLength = 0;
    }

    public String getTableName() {
        return tableName;
    }

    public Integer getRowLength() {
        return rowLength;
    }

    public ArrayList<Attribute> getAttributes() {
        return attributes;
    }

    @Override
    public String toString() {
        return "Table{" +
                "rowLength=" + rowLength +
                ", attributes=" + attributes +
                ", tableName='" + tableName + '\'' +
                '}';
    }
}
