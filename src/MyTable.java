import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.util.ArrayList;

public class MyTable extends JPanel {

    ArrayList<Output> outputs;

    public MyTable(ArrayList<Output> outputs){
        this.outputs = outputs;

        setLayout(new BorderLayout());

        setBounds(0, 0, 600, 400);

        JPanel centerPanel = new JPanel(new GridLayout(outputs.size()+1,outputs.get(0).getSize()));

        ArrayList<JLabel> labels = new ArrayList<>();

        int k=0;
        for(int j=0;j<outputs.get(0).getSize();j++){
            labels.add(new JLabel(outputs.get(0).getName(j)));
            labels.get(k).setBorder(BorderFactory.createLineBorder(Color.BLACK));
            labels.get(k).setHorizontalAlignment(SwingConstants.CENTER);
            labels.get(k).setVerticalAlignment(SwingConstants.CENTER);
            labels.get(k).setFont(new Font("Times New Roman", Font.PLAIN, 20));
            centerPanel.add(labels.get(k));
            k++;
        }

        if(outputs.get(0).valSize() != 0){
            for(int i = 0;i<outputs.size();i++){
                for(int j=0; j<outputs.get(i).getSize();j++){
                    labels.add(new JLabel(outputs.get(i).getVal(j)));
                    labels.get(k).setBorder(BorderFactory.createLineBorder(Color.BLACK));
                    labels.get(k).setHorizontalAlignment(SwingConstants.CENTER);
                    labels.get(k).setVerticalAlignment(SwingConstants.CENTER);
                    labels.get(k).setFont(new Font("Times New Roman", Font.PLAIN, 20));
                    centerPanel.add(labels.get(k));
                    k++;
                }
            }
        } else {
            for(int j=0; j<outputs.get(0).getSize();j++){
                labels.add(new JLabel(""));
                labels.get(k).setBorder(BorderFactory.createLineBorder(Color.BLACK));
                labels.get(k).setHorizontalAlignment(SwingConstants.CENTER);
                labels.get(k).setVerticalAlignment(SwingConstants.CENTER);
                labels.get(k).setFont(new Font("Times New Roman", Font.PLAIN, 20));
                centerPanel.add(labels.get(k));
                k++;
            }
        }

        add(new JScrollPane(centerPanel));

        setVisible(true);
    }

    public MyTable() {
        this.outputs = new ArrayList<>();

        setLayout(new BorderLayout());

        setBounds(0, 0, 600, 400);


        setVisible(true);
    }

    public ArrayList<Output> getOutput(){

        Output output = new Output("name1 name2", "val1 val2");
        Output output1 = new Output("name1 name2", "val3 val4");
        Output output2 = new Output("name1 name2", "val5 val6");

        ArrayList<Output> arrayList = new ArrayList<>(3);
        arrayList.add(output);
        arrayList.add(output1);
        arrayList.add(output2);

        return arrayList;
    }
}