import java.util.ArrayList;

public class Index {

    private final String tableName;
    private final String indexType;
    private final boolean isUnique;
    private final int keyLength;
    private final String indexName;
    private final String attributeName;

    public Index(String tableName, boolean isUnique, int keyLength, String indexName, String attributeName) {

        this.tableName = tableName;
        indexType = "MongoDB";
        this.isUnique = isUnique;
        this.keyLength = keyLength;
        this.indexName = indexName;
        this.attributeName = attributeName;

    }

    public String getTableName() {
        return tableName;
    }

    public String getIndexType() {
        return indexType;
    }

    public boolean isUnique() {
        return isUnique;
    }

    public int getKeyLength() {
        return keyLength;
    }

    public String getIndexName() {
        return indexName;
    }

    public String getAttributeName() {
        return attributeName;
    }

    @Override
    public String toString() {
        return "Index{" +
                "tableName='" + tableName + '\'' +
                ", indexType='" + indexType + '\'' +
                ", isUnique=" + isUnique +
                ", keyLength=" + keyLength +
                ", indexName='" + indexName + '\'' +
                ", attributeNames=" + attributeName +
                "}\n";
    }
}
