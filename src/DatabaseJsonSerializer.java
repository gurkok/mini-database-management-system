import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.databind.SerializerProvider;
import com.fasterxml.jackson.databind.ser.std.StdSerializer;

import javax.xml.crypto.Data;
import java.io.IOException;
import java.util.ArrayList;

public class DatabaseJsonSerializer extends StdSerializer<ArrayList<Database>> {


    protected DatabaseJsonSerializer() {
        super(ArrayList.class, true);
    }

    @Override
    public void serialize(ArrayList<Database> databases, JsonGenerator jsonGenerator, SerializerProvider serializerProvider) throws IOException {
        jsonGenerator.writeStartObject();
        jsonGenerator.writeArrayFieldStart("databases");

        for (Database database : databases) {
            jsonGenerator.writeStartObject();
            jsonGenerator.writeStringField("databaseName", database.getDatabaseName());
            jsonGenerator.writeArrayFieldStart("tables");
            if (!database.getTables().isEmpty()) {
                for (Table table : database.getTables()) {
                    jsonGenerator.writeStartObject();
                    jsonGenerator.writeStringField("rowLength", table.getRowLength().toString());
                    jsonGenerator.writeStringField("tableName", table.getTableName());
                    jsonGenerator.writeArrayFieldStart("Structure");

                    for (Attribute attribute : table.getAttributes()) {
                        jsonGenerator.writeStartObject();
                        jsonGenerator.writeStringField("isnull", String.valueOf(attribute.isNull()));
                        jsonGenerator.writeStringField("isPrimaryKey", String.valueOf(attribute.isPrimaryKey()));
                        jsonGenerator.writeStringField("length", String.valueOf(attribute.getLength()));
                        jsonGenerator.writeStringField("type", attribute.getType());
                        jsonGenerator.writeStringField("isunique", String.valueOf(attribute.isUnique()));
                        jsonGenerator.writeStringField("attributeName", attribute.getAttributeName());
                        jsonGenerator.writeStringField("foreignTable", attribute.getForeignTable());
                        jsonGenerator.writeStringField("foreignAttribute", attribute.getForeignAttribute());
                        jsonGenerator.writeEndObject();
                    }
                    jsonGenerator.writeEndArray();

                    jsonGenerator.writeEndObject();
                }
            }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeArrayFieldStart("indexes");

            if (!database.getIndexes().isEmpty()) {
                for (Index index : database.getIndexes()) {

                    jsonGenerator.writeStartObject();
                    jsonGenerator.writeStringField("tableName", index.getTableName());
                    jsonGenerator.writeStringField("type", index.getIndexType());
                    jsonGenerator.writeStringField("isUnique", String.valueOf(index.isUnique()));
                    jsonGenerator.writeStringField("keyLength", String.valueOf(index.getKeyLength()));
                    jsonGenerator.writeStringField("indexName", index.getIndexName());
                    jsonGenerator.writeStringField("indexAttributeName", index.getAttributeName());
                    jsonGenerator.writeEndObject();

                }
            }
            jsonGenerator.writeEndArray();
            jsonGenerator.writeEndObject();
        }
        jsonGenerator.writeEndArray();
        jsonGenerator.writeEndObject();
    }
}
